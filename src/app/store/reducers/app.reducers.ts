import { ActionReducerMap } from "@ngrx/store";

import { routerReducer } from "@ngrx/router-store";
import { IAppState } from "../state/app.state";
import { authReducers } from "src/app/auth/auth.reducers";
import { albumReducers } from "src/app/album/album.reducers";
// import { configReducers } from './config.reducers';
// import { userReducers } from './user.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
  router: routerReducer,
  auth: authReducers,
  album: albumReducers
};
