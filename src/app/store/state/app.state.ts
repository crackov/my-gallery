import { RouterReducerState } from "@ngrx/router-store";
import { IAuthState, initialAuthState } from "src/app/auth/auth.state";
import { IAlbumState, initialAlbumState } from "src/app/album/album.state";

export interface IAppState {
  router?: RouterReducerState;
  auth: IAuthState;
  album: IAlbumState;
}

export const initialAppState: IAppState = {
  auth: initialAuthState,
  album: initialAlbumState
};

export function getInitialState(): IAppState {
  return initialAppState;
}
