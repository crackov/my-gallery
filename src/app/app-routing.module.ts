import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { AuthGuard } from "./guards/auth.guard";
import { AnonGuard } from "./guards/anon.guard";

const routes: Routes = [
  { path: "", redirectTo: "auth", pathMatch: "full" },
  {
    path: "auth",
    loadChildren: "./auth/auth.module#AuthModule",
    canActivate: [AnonGuard]
  },
  {
    path: "albums",
    loadChildren: "./album/album.module#AlbumModule",
    canActivate: [AuthGuard]
  },
  { path: "**", redirectTo: "auth", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
