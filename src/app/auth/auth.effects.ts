import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from "@ngrx/effects";
import { of } from "rxjs";
import { switchMap, map } from "rxjs/operators";

import { ILoginAttempt, ILoginResponse } from "./auth.interface";
import {
  LoginAttempt,
  EAuthActions,
  LoginSuccess,
  LoginFailiure,
  LogoutStart,
  LogoutComplete
} from "./auth.actions";
import { AuthService } from "./auth.service";
import { Router } from "@angular/router";

@Injectable()
export class AuthEffects {
  @Effect()
  login$ = this._actions$.pipe(
    ofType<LoginAttempt>(EAuthActions.LoginAttempt),
    map((action: LoginAttempt) => action.payload),
    switchMap((payload: ILoginAttempt) => this._authService.logIn(payload)),
    switchMap((response: ILoginResponse) => {
      if ("email" in response) {
        localStorage.setItem("my-gallery-email", response.email);
        localStorage.setItem("my-gallery-token", response.token);
        this._router.navigate(["/albums"]);
        return of(new LoginSuccess(response));
      } else {
        return of(new LoginFailiure(response));
      }
    })
  );

  @Effect()
  logout$ = this._actions$.pipe(
    ofType<LogoutStart>(EAuthActions.LogoutStart),
    switchMap(() => of(this._authService.logOut())),
    switchMap(() => of(new LogoutComplete()))
  );

  constructor(
    private _authService: AuthService,
    private _actions$: Actions,
    private _router: Router
  ) {}
}
