import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LoginComponent } from "./layouts/login/login.component";
import { RouterModule, Routes } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { AnonGuard } from "../guards/anon.guard";

const routes: Routes = [
  { path: "", component: LoginComponent, canActivate: [AnonGuard] }
];

@NgModule({
  declarations: [LoginComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ReactiveFormsModule]
})
export class AuthModule {}
