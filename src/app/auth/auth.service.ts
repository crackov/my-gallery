import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ILoginAttempt, ILoginResponse } from "./auth.interface";
import { Observable, of } from "rxjs";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor() {}

  logIn(payload: ILoginAttempt): Observable<ILoginResponse> {
    return of({
      email: payload.email,
      token: "test-token-example"
    });
  }

  logOut() {
    localStorage.removeItem("my-gallery-email");
    localStorage.removeItem("my-gallery-token");
  }

  isLoggedIn() {
    return false;
  }
}
