import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { AuthService } from "src/app/auth/auth.service";
import { Store } from "@ngrx/store";
import { IAppState } from "src/app/store/state/app.state";
import { LoginAttempt } from "../../auth.actions";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm;

  constructor(
    private formBuilder: FormBuilder,
    private _store: Store<IAppState>
  ) {}

  get lf() {
    return this.loginForm.controls;
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required])
    });
  }

  onLogin() {
    if (this.loginForm.valid) {
      this._store.dispatch(new LoginAttempt(this.loginForm.value));
    }
  }
}
