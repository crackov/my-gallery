import { IUser } from "./auth.interface";

export interface IAuthState {
  currentUser?: IUser;
  isLoading: boolean;
  error?: string;
}

const email: string = localStorage.getItem("my-gallery-email");
const token: string = localStorage.getItem("my-gallery-token");

export const initialAuthState: IAuthState = {
  currentUser: email &&
    token && {
      email,
      token
    },
  isLoading: false,
  error: null
};
