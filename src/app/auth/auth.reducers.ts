import { EAuthActions, AuthActions } from "./auth.actions";
import { initialAuthState, IAuthState } from "./auth.state";

export const authReducers = (
  state = initialAuthState,
  action: AuthActions
): IAuthState => {
  switch (action.type) {
    case EAuthActions.LoginSuccess: {
      return {
        ...state,
        currentUser: action.payload,
        error: null
      };
    }
    case EAuthActions.LoginFailiure: {
      return {
        ...state,
        currentUser: null,
        error: action.payload.error
      };
    }
    case EAuthActions.LogoutComplete: {
      return {
        ...state,
        currentUser: null
      };
    }

    default:
      return state;
  }
};
