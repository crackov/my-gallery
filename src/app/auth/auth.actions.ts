import { Action } from "@ngrx/store";
import { ILoginAttempt, ILoginSuccess } from "./auth.interface";
import { IErrorResponse } from "../app.interface";

export enum EAuthActions {
  LoginAttempt = "[Auth] Login Attempt",
  LoginSuccess = "[Auth] Login Success",
  LoginFailiure = "[Auth] Login Failiure",
  LogoutStart = "[Auth] Logout Start",
  LogoutComplete = "[Auth] Logout Complete"
}

export class LoginAttempt implements Action {
  public readonly type = EAuthActions.LoginAttempt;
  constructor(public payload: ILoginAttempt) {}
}

export class LoginSuccess implements Action {
  public readonly type = EAuthActions.LoginSuccess;
  constructor(public payload: ILoginSuccess) {}
}

export class LoginFailiure implements Action {
  public readonly type = EAuthActions.LoginFailiure;
  constructor(public payload: IErrorResponse) {}
}

export class LogoutStart implements Action {
  public readonly type = EAuthActions.LogoutStart;
}

export class LogoutComplete implements Action {
  public readonly type = EAuthActions.LogoutComplete;
}

export type AuthActions =
  | LoginAttempt
  | LoginSuccess
  | LoginFailiure
  | LogoutStart
  | LogoutComplete;
