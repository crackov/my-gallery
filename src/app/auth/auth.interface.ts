import { IErrorResponse } from "../app.interface";

export interface ILoginAttempt {
  email: string;
  password: string;
}

export interface ILoginSuccess {
  email: string;
  token: string;
}

export type ILoginResponse = ILoginSuccess | IErrorResponse;

export interface IUser {
  email: string;
  token: string;
}
