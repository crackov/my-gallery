import { Injectable } from "@angular/core";
import { Effect, ofType, Actions } from "@ngrx/effects";
import { of } from "rxjs";
import { switchMap, map } from "rxjs/operators";
import {
  LoadCollectionAttempt,
  EAlbumActions,
  LoadCollectionFailiure,
  LoadCollectionSuccess,
  ChangeLayout,
  LoadAlbumAttempt,
  LoadAlbumFailiure,
  LoadAlbumSuccess
} from "./album.actions";
import { AlbumService } from "./album.service";
import { ILoadCollectionResponse, ILoadAlbumResponse } from "./album.interface";

@Injectable()
export class AlbumEffects {
  @Effect()
  LoadCollection$ = this._actions$.pipe(
    ofType<LoadCollectionAttempt>(EAlbumActions.LoadCollectionAttempt),
    switchMap((action: LoadCollectionAttempt) =>
      this._albumService.loadCollection(action.payload.page)
    ),
    switchMap((response: ILoadCollectionResponse) => {
      if ("error" in response) {
        return of(new LoadCollectionFailiure(response));
      } else {
        return of(new LoadCollectionSuccess(response));
      }
    })
  );

  @Effect()
  LoadAlbum$ = this._actions$.pipe(
    ofType<LoadAlbumAttempt>(EAlbumActions.LoadAlbumAttempt),
    switchMap((action: LoadAlbumAttempt) =>
      this._albumService.loadAlbum(action.payload)
    ),
    switchMap((response: ILoadAlbumResponse) => {
      if ("error" in response) {
        return of(new LoadAlbumFailiure(response));
      } else {
        return of(new LoadAlbumSuccess(response));
      }
    })
  );

  @Effect({ dispatch: false })
  ChangeLayout$ = this._actions$.pipe(
    ofType<ChangeLayout>(EAlbumActions.ChangeLayout),
    map((response: ChangeLayout) => {
      localStorage.setItem("my-gallery-isGrid", `${response.payload.isGrid}`);
    })
  );

  constructor(
    private _albumService: AlbumService,
    private _actions$: Actions
  ) {}
}
