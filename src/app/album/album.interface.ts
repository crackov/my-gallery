import { IErrorResponse } from "../app.interface";

export interface IChangeLayout {
  isGrid: boolean;
}

export interface IAlbum {
  id: number;
  title: string;
  author: string;
  photoUrl: string;
}

export interface ILoadAlbumPayload {
  albumId: number;
  firstLoad: boolean;
  page?: number;
}

export interface ILoadCollectionAttempt {
  page?: number;
  firstLoad?: boolean;
}

export interface ILoadCollectionSuccess {
  items: Array<IAlbum>;
  noOfPages: number;
}

export interface ILoadAlbumSuccess {
  singleAlbum: IAlbum;
  items: Array<IPhoto>;
  noOfPages: number;
}

export type ILoadCollectionResponse = ILoadCollectionSuccess | IErrorResponse;

export type ILoadAlbumResponse = ILoadAlbumSuccess | IErrorResponse;

export interface IPhoto {
  albumId: number;
  id: number;
  title: string;
  photoUrl: string;
}

export interface IFilter {
  value: string;
}
