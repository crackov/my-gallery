import { Component, OnInit, HostListener } from "@angular/core";
import { Store } from "@ngrx/store";
import { LoadCollectionAttempt } from "../../album.actions";
import { IAlbum } from "../../album.interface";
import { IAppState } from "src/app/store/state/app.state";

@Component({
  selector: "app-album-collection",
  templateUrl: "./album-collection.component.html",
  styleUrls: ["./album-collection.component.scss"]
})
export class AlbumCollectionComponent implements OnInit {
  private collection: Array<IAlbum> = new Array();
  private showingPage: number = 0;
  private page: number = 0;
  private lastPage: number = 1;
  private isLoading: boolean = false;

  constructor(private _store: Store<IAppState>) {}

  ngOnInit() {
    this._store
      .select((state: IAppState) => state.album)
      .subscribe(data => {
        this.collection = data.collection;
        this.page = data.currentPage;
        this.lastPage = data.lastPage;
        this.isLoading = data.isLoading;
      });

    this._store.dispatch(
      new LoadCollectionAttempt({ page: this.page + 1, firstLoad: true })
    );
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    const height = document.documentElement.scrollHeight;
    if (
      window.innerHeight + window.scrollY >= height - 600 &&
      this.showingPage <= this.page &&
      this.page < this.lastPage
    ) {
      this.showingPage++;
      this._store.dispatch(new LoadCollectionAttempt({ page: this.page + 1 }));
    }
  }
}
