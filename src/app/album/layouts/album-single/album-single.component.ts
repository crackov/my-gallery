import { Component, OnInit, HostListener } from "@angular/core";
import { IPhoto, IAlbum } from "../../album.interface";
import { Store } from "@ngrx/store";
import { IAppState } from "src/app/store/state/app.state";
import { LoadAlbumAttempt } from "../../album.actions";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-album-single",
  templateUrl: "./album-single.component.html",
  styleUrls: ["./album-single.component.scss"]
})
export class AlbumSingleComponent implements OnInit {
  private album: Array<IPhoto> = [];
  private collection: Array<IAlbum> = [];
  private showingPage: number = 0;
  private page: number = 0;
  private lastPage: number = 1;
  private isLoading: boolean = false;
  private title: string;
  private _showAreYouSure: boolean = false;
  private _showSinglePhoto: boolean = false;
  private currentUrl: string = "";

  constructor(
    private _store: Store<IAppState>,
    private _route: ActivatedRoute
  ) {}

  ngOnInit() {
    this._store
      .select((state: IAppState) => state.album)
      .subscribe(data => {
        const currentAlbum = data.collection.find(
          al => al.id === parseInt(this._route.snapshot.paramMap.get("albumId"))
        ) || { title: "" };
        this.album = data.album;
        this.collection = data.collection;
        this.page = data.currentPage;
        this.lastPage = data.lastPage;
        this.isLoading = data.isLoading;
        this.title = currentAlbum.title;
      });

    this._store.dispatch(
      new LoadAlbumAttempt({
        albumId: parseInt(this._route.snapshot.paramMap.get("albumId")),
        firstLoad: true
      })
    );
  }

  @HostListener("window:scroll", [])
  onScroll(): void {
    const height = document.documentElement.scrollHeight;
    if (
      window.innerHeight + window.scrollY >= height - 600 &&
      this.showingPage <= this.page &&
      this.page < this.lastPage
    ) {
      this.showingPage++;
      this._store.dispatch(
        new LoadAlbumAttempt({
          page: this.page + 1,
          firstLoad: false,
          albumId: parseInt(this._route.snapshot.paramMap.get("albumId"))
        })
      );
    }
  }

  showAreYouSure() {
    this._showAreYouSure = true;
  }

  hideAreYouSure() {
    this._showAreYouSure = false;
  }

  showSinglePhoto(url) {
    this._showSinglePhoto = true;
    this.currentUrl = url;
  }

  hideSinglePhoto() {
    this._showSinglePhoto = false;
  }
}
