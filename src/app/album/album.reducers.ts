import { EAlbumActions, AlbumActions } from "./album.actions";
import { initialAlbumState, IAlbumState } from "./album.state";

export const albumReducers = (
  state = initialAlbumState,
  action: AlbumActions
): IAlbumState => {
  switch (action.type) {
    case EAlbumActions.ChangeLayout: {
      return {
        ...state,
        isGrid: action.payload.isGrid
      };
    }
    case EAlbumActions.LoadCollectionAttempt: {
      const condState = action.payload.firstLoad
        ? {
            currentPage: 0,
            lastPage: 1,
            collection: []
          }
        : {};
      return {
        ...state,
        ...condState,
        error: null,
        isLoading: true
      };
    }
    case EAlbumActions.LoadCollectionSuccess: {
      return {
        ...state,
        error: null,
        isLoading: false,
        collection: [...state.collection, ...action.payload.items],
        currentPage: state.currentPage + 1,
        lastPage: action.payload.noOfPages
      };
    }
    case EAlbumActions.LoadCollectionFailiure: {
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
        collection: null
      };
    }
    case EAlbumActions.LoadAlbumAttempt: {
      const condState = action.payload.firstLoad
        ? {
            album: [],
            currentPage: 1,
            lastPage: 1,
            filterValue: ""
          }
        : {};

      return {
        ...state,
        ...condState,
        error: null,
        isLoading: true
      };
    }
    case EAlbumActions.LoadAlbumSuccess: {
      return {
        ...state,
        error: null,
        isLoading: false,
        album: [...state.album, ...action.payload.items],
        currentPage: state.currentPage + 1,
        lastPage: action.payload.noOfPages,
        collection: state.collection.find(
          al => al.id === action.payload.singleAlbum.id
        )
          ? state.collection
          : [action.payload.singleAlbum]
      };
    }
    case EAlbumActions.LoadAlbumFailiure: {
      return {
        ...state,
        error: action.payload.error,
        isLoading: false,
        album: null
      };
    }
    case EAlbumActions.FilterPhotos: {
      return {
        ...state,
        filterValue: action.payload.value
      };
    }

    default:
      return state;
  }
};
