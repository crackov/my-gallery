import { Component, OnInit, Input } from "@angular/core";
import { IAlbumState } from "../../album.state";
import { IAppState } from "src/app/store/state/app.state";
import { Store } from "@ngrx/store";
import { ChangeLayout } from "../../album.actions";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  @Input() title: string;
  isGrid: boolean = false;

  constructor(private _store: Store<IAppState>) {}

  ngOnInit() {
    this._store
      .select((state: IAppState) => state.album.isGrid)
      .subscribe(data => {
        this.isGrid = data;
      });
  }

  showGrid() {
    this._store.dispatch(new ChangeLayout({ isGrid: true }));
  }

  showList() {
    this._store.dispatch(new ChangeLayout({ isGrid: false }));
  }
}
