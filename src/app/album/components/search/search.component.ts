import { Component, OnInit } from "@angular/core";
import { Store } from "@ngrx/store";
import { IAppState } from "src/app/store/state/app.state";
import { FilterPhotos } from "../../album.actions";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
  constructor(private _store: Store<IAppState>) {}

  ngOnInit() {}

  onChange(value) {
    this._store.dispatch(new FilterPhotos({ value }));
  }
}
