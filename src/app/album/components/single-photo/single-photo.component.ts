import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-single-photo",
  templateUrl: "./single-photo.component.html",
  styleUrls: ["./single-photo.component.scss"]
})
export class SinglePhotoComponent implements OnInit {
  @Input() url = "";
  @Output() onClose = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  close() {
    this.onClose.emit();
  }
}
