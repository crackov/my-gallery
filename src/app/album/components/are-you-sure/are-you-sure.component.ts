import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "app-are-you-sure",
  templateUrl: "./are-you-sure.component.html",
  styleUrls: ["./are-you-sure.component.scss"]
})
export class AreYouSureComponent implements OnInit {
  @Input() name: string = "";
  @Output() onClose = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  close() {
    this.onClose.emit();
  }
}
