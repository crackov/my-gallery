import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Store } from "@ngrx/store";
import { IAppState } from "src/app/store/state/app.state";

@Component({
  selector: "app-album-grid",
  templateUrl: "./album-grid.component.html",
  styleUrls: ["./album-grid.component.scss"]
})
export class AlbumGridComponent implements OnInit {
  @Input() items: any;
  @Output() onDelete = new EventEmitter();
  @Output() onOpen = new EventEmitter();

  isGrid: boolean = true;

  constructor(private _store: Store<IAppState>) {}

  ngOnInit() {
    this._store
      .select((state: IAppState) => state.album.isGrid)
      .subscribe(data => {
        this.isGrid = data;
      });
  }

  open(url) {
    this.onOpen.emit(url);
  }

  delete() {
    this.onDelete.emit();
  }
}
