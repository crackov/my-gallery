import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { HeaderComponent } from "./components/header/header.component";
import { AlbumGridComponent } from "./components/album-grid/album-grid.component";
import { AlbumCollectionComponent } from "./layouts/album-collection/album-collection.component";
import { AlbumSingleComponent } from "./layouts/album-single/album-single.component";
import { HttpClientModule } from "@angular/common/http";
import { NavigationComponent } from './components/navigation/navigation.component';
import { SearchComponent } from './components/search/search.component';
import { FilterPipe } from './filter.pipe';
import { AreYouSureComponent } from './components/are-you-sure/are-you-sure.component';
import { SinglePhotoComponent } from './components/single-photo/single-photo.component';

const routes: Routes = [
  { path: "", component: AlbumCollectionComponent },
  { path: ":albumId", component: AlbumSingleComponent }
  // { path: ":albumId/images/:imageId", component: ImageComponent }
];

@NgModule({
  declarations: [
    HeaderComponent,
    AlbumGridComponent,
    AlbumCollectionComponent,
    AlbumSingleComponent,
    NavigationComponent,
    SearchComponent,
    FilterPipe,
    AreYouSureComponent,
    SinglePhotoComponent
  ],
  imports: [CommonModule, RouterModule.forChild(routes), HttpClientModule]
})
export class AlbumModule {}
