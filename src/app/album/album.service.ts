import { Injectable } from "@angular/core";
import {
  IAlbum,
  ILoadCollectionResponse,
  IPhoto,
  ILoadAlbumResponse,
  ILoadAlbumPayload
} from "./album.interface";
import { Observable, of, throwError, forkJoin } from "rxjs";
import {
  HttpClient,
  HttpErrorResponse,
  HttpResponse
} from "@angular/common/http";
import { map, catchError, switchMap, single } from "rxjs/operators";
import { Store } from "@ngrx/store";
import { IAppState } from "../store/state/app.state";
import * as parseLink from "parse-link-header";
import * as faker from "faker";

@Injectable({
  providedIn: "root"
})
export class AlbumService {
  constructor(private http: HttpClient, private _store: Store<IAppState>) {}

  loadCollection(page = 1): Observable<ILoadCollectionResponse> {
    return this._store
      .select(
        (state: IAppState) =>
          state.auth.currentUser && state.auth.currentUser.token
      )
      .pipe(
        switchMap((token?: string) => {
          return this.http.get<Array<IAlbum>>(
            `https://jsonplaceholder.typicode.com/albums?_page=${page}`,
            {
              observe: "response",
              headers: token && {
                Authorization: `Bearer ${token}`
              }
            }
          );
        }),
        map((res: HttpResponse<Array<IAlbum>>) => {
          const link = parseLink(res.headers.get("Link"));
          return {
            items: res.body.map(item => {
              item.photoUrl = `https://picsum.photos/270/150?force=${Math.random()
                .toString(36)
                .substring(7)}`;
              item.author = faker.name.firstName();
              return item;
            }),
            noOfPages: parseInt(link.last._page)
          };
        }),
        catchError((err: HttpErrorResponse) =>
          throwError({ error: err.message })
        )
      );
  }

  loadAlbum(
    payload: ILoadAlbumPayload,
    page: number = 1
  ): Observable<ILoadAlbumResponse> {
    const getSingleAlbum = (id, page, token) =>
      this.http.get<IAlbum>(
        `https://jsonplaceholder.typicode.com/albums/${id}`,
        {
          observe: "response",
          headers: token && {
            Authorization: `Bearer ${token}`
          }
        }
      );

    const getPhotos = (id, page, token) =>
      this.http.get<Array<IPhoto>>(
        `https://jsonplaceholder.typicode.com/albums/${id}/photos?_page=${page}`,
        {
          observe: "response",
          headers: token && {
            Authorization: `Bearer ${token}`
          }
        }
      );

    return this._store
      .select(
        (state: IAppState) =>
          state.auth.currentUser && state.auth.currentUser.token
      )
      .pipe(
        switchMap((token?: string) => {
          return forkJoin([
            getSingleAlbum(payload.albumId, page, token),
            getPhotos(payload.albumId, page, token)
          ]);
        }),
        map(resArray => {
          const singleAlbum = resArray[0];
          const photos = resArray[1];

          const link = parseLink(photos.headers.get("Link"));
          return {
            singleAlbum: singleAlbum.body,
            items: photos.body.map(item => {
              item.photoUrl = `https://picsum.photos/270/150?force=${Math.random()
                .toString(36)
                .substring(7)}`;
              // item.author = faker.name.firstName();
              return item;
            }),
            noOfPages: parseInt(link.last._page)
          };
        }),
        catchError((err: HttpErrorResponse) =>
          throwError({ error: err.message })
        )
      );
  }
}
