import { IAlbum, IPhoto } from "./album.interface";

export interface IAlbumState {
  isGrid: boolean;
  isLoading: boolean;
  error?: string;
  collection?: Array<IAlbum>;
  album?: Array<IPhoto>;
  currentPage: number;
  lastPage: number;
  filterValue: string;
}

function getDefaultIsGrid(): boolean {
  const stored = localStorage.getItem("my-gallery-isGrid");

  if (!stored) return true;

  return stored === "true";
}

export const initialAlbumState: IAlbumState = {
  isGrid: getDefaultIsGrid(),
  isLoading: false,
  error: null,
  collection: [],
  album: [],
  currentPage: 0,
  lastPage: 1,
  filterValue: ""
};
