import { Pipe, PipeTransform } from "@angular/core";
import { Store } from "@ngrx/store";
import { IAppState } from "../store/state/app.state";

@Pipe({
  name: "filter",
  pure: false
})
export class FilterPipe implements PipeTransform {
  private _filter = "";
  constructor(private _store: Store<IAppState>) {
    this._store
      .select(state => state.album.filterValue)
      .subscribe(filter => {
        this._filter = filter;
      });
  }

  transform(value: Array<any>, ...args: any[]): any {
    return value.filter(el => {
      return this._filter !== "" ? el.title.includes(this._filter) : true;
    });
  }
}
