import { Action } from "@ngrx/store";
import {
  IChangeLayout,
  ILoadCollectionSuccess,
  ILoadCollectionAttempt,
  IFilter,
  ILoadAlbumSuccess,
  ILoadAlbumPayload
} from "./album.interface";
import { IErrorResponse } from "../app.interface";

export enum EAlbumActions {
  ChangeLayout = "[Album] Change Layout",
  LoadCollectionAttempt = "[Album] Load Collection Attempt",
  LoadCollectionSuccess = "[Album] Load Collection Success",
  LoadCollectionFailiure = "[Album] Load Collection Failiure",
  LoadAlbumAttempt = "[Album] Load Album Attempt",
  LoadAlbumSuccess = "[Album] Load Album Success",
  LoadAlbumFailiure = "[Album] Load Album Failiure",
  FilterPhotos = "[Album] Filter Photos"
}

export class ChangeLayout implements Action {
  public readonly type = EAlbumActions.ChangeLayout;
  constructor(public payload: IChangeLayout) {}
}

export class LoadCollectionAttempt implements Action {
  public readonly type = EAlbumActions.LoadCollectionAttempt;
  constructor(public payload: ILoadCollectionAttempt = {}) {}
}

export class LoadCollectionSuccess implements Action {
  public readonly type = EAlbumActions.LoadCollectionSuccess;
  constructor(public payload: ILoadCollectionSuccess) {}
}

export class LoadCollectionFailiure implements Action {
  public readonly type = EAlbumActions.LoadCollectionFailiure;
  constructor(public payload: IErrorResponse) {}
}

export class LoadAlbumAttempt implements Action {
  public readonly type = EAlbumActions.LoadAlbumAttempt;
  constructor(public payload: ILoadAlbumPayload) {}
}

export class LoadAlbumSuccess implements Action {
  public readonly type = EAlbumActions.LoadAlbumSuccess;
  constructor(public payload: ILoadAlbumSuccess) {}
}

export class LoadAlbumFailiure implements Action {
  public readonly type = EAlbumActions.LoadAlbumFailiure;
  constructor(public payload: IErrorResponse) {}
}

export class FilterPhotos implements Action {
  public readonly type = EAlbumActions.FilterPhotos;
  constructor(public payload: IFilter) {}
}

export type AlbumActions =
  | ChangeLayout
  | LoadCollectionAttempt
  | LoadCollectionSuccess
  | LoadCollectionFailiure
  | LoadAlbumAttempt
  | LoadAlbumSuccess
  | LoadAlbumFailiure
  | FilterPhotos;
