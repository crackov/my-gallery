import { Injectable } from "@angular/core";
import {
  UrlTree,
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth.service";
import { Store } from "@ngrx/store";
import { IAppState } from "../store/state/app.state";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private _store: Store<IAppState>, private _router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._store
      .select(state => state.auth.currentUser)
      .pipe(
        map(currentUser =>
          currentUser ? true : this._router.createUrlTree(["/auth"])
        )
      );
  }
}
