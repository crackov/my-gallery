import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../auth/auth.service";
import { Store } from "@ngrx/store";
import { IAppState } from "../store/state/app.state";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AnonGuard implements CanActivate {
  constructor(private _store: Store<IAppState>, private _router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this._store
      .select(state => state.auth.currentUser)
      .pipe(
        map(currentUser =>
          !currentUser ? true : this._router.createUrlTree(["/albums"])
        )
      );
  }
}
